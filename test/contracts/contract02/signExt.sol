// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Strings.sol)

pragma solidity > 0.4.22;

contract x {
    function signExtend(int8 value, int256 ext) public pure returns (int256) {
        int256 y;
        assembly {
            let result := signextend(ext, value)
            y := result
        }

        return y;
    }
}
