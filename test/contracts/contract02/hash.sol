// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract hashtest {
  function hashMatched(string memory hashLock, string memory secret) public pure returns (bool) {
    bytes memory a = abi.encodePacked(sha256(bytes(secret)));
    bytes memory b = hexStringToBytes(hashLock);
    return sha256(a) == sha256(b);
  }
  function hexStringToBytes(string memory hexStr) internal pure returns (bytes memory) {
    string memory hexStrWithoutPrefix = removePrefix(hexStr);
    bytes memory bts = bytes(hexStrWithoutPrefix);
    require(bts.length % 2 == 0);
    bytes memory result = new bytes(bts.length / 2);
    uint256 len = bts.length / 2;
    for (uint256 i = 0; i < len; ++i) {
      result[i] = bytes1(fromHexChar(uint8(bts[2 * i])) * 16 + fromHexChar(uint8(bts[2 * i + 1])));
    }
    return result;
  }
  function removePrefix(string memory hexStr) internal pure returns (string memory) {
    // remove 0x frefix
    bytes memory strBytes = bytes(hexStr);
    if (strBytes.length > 2 && strBytes[1] == "x") {
      return substring(hexStr, 2, strBytes.length);
    } else {
      return hexStr;
    }
  }
  function substring(string memory str, uint256 startIndex, uint256 endIndex) internal pure returns (string memory) {
    bytes memory strBytes = bytes(str);
    bytes memory result = new bytes(endIndex - startIndex);
    for (uint256 i = startIndex; i < endIndex; i++) {
      result[i - startIndex] = strBytes[i];
    }
    return string(result);
  }
  function fromHexChar(uint8 c) internal pure returns (uint8) {
    if (bytes1(c) >= bytes1("0") && bytes1(c) <= bytes1("9")) {
      return c - uint8(bytes1("0"));
    }
    if (bytes1(c) >= bytes1("a") && bytes1(c) <= bytes1("f")) {
      return 10 + c - uint8(bytes1("a"));
    }
    if (bytes1(c) >= bytes1("A") && bytes1(c) <= bytes1("F")) {
      return 10 + c - uint8(bytes1("A"));
    }
  }
}

