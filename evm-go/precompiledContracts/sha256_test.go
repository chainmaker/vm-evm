package precompiledContracts

import (
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/environment"
	"encoding/hex"
	"reflect"
	"testing"
)

func Test_sha256hash_Execute(t *testing.T) {
	want, _ := hex.DecodeString("2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824")
	type args struct {
		input   []byte
		version uint32
		ctx     *environment.Context
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			"test sha256",
			args{
				[]byte("hello"),
				2030400,
				&environment.Context{},
			},
			want,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &sha256hash{}
			got, err := c.Execute(tt.args.input, tt.args.version, tt.args.ctx)
			str := hex.EncodeToString(got)
			if (err != nil) != tt.wantErr {
				t.Errorf("Execute() error = %v, wantErr %v, %v", err, tt.wantErr, str)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Execute() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sha256hash_GasCost(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want uint64
	}{
		{
			"test gas",
			args{
				[]byte("hello"),
			},
			72,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &sha256hash{}
			if got := c.GasCost(tt.args.input); got != tt.want {
				t.Errorf("GasCost() = %v, want %v", got, tt.want)
			}
		})
	}
}
