package precompiledContracts

import (
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/environment"
	"encoding/hex"
	"reflect"
	"testing"
)

func Test_runEcRecover(t *testing.T) {
	sig, _ := hex.DecodeString("38d18acb67d25c8bb9942764b62f18e17054f66a817bd4295423adf9ed98873e000000000000000000000000000000000000000000000000000000000000001b38d18acb67d25c8bb9942764b62f18e17054f66a817bd4295423adf9ed98873e789d1dd423d25f0772d2748d60f7e4b81bb14d086eba8e8e8efb6dcff8a4ae02")

	type args struct {
		input []byte
	}

	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "test1",
			args: args{
				input: sig,
				//Expected: "000000000000000000000000ceaccac640adf55b2028469bd36ba501f28b699d",
			},
			want:    "ceaccac640adf55b2028469bd36ba501f28b699d",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := ecRecover{}
			got, err := e.Execute(tt.args.input, 3000000, &environment.Context{})
			if (err != nil) != tt.wantErr {
				t.Errorf("Execute() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			ret := hex.EncodeToString(got)
			if !reflect.DeepEqual(ret[24:], tt.want) {
				t.Errorf("Execute() got = %v, want %v", ret, tt.want)
			}
		})
	}
}
