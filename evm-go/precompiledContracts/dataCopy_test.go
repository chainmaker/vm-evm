package precompiledContracts

import (
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/environment"
	"reflect"
	"testing"
)

func Test_dataCopy_Execute(t *testing.T) {
	type args struct {
		in      []byte
		version uint32
		ctx     *environment.Context
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			"test dataCopy",
			args{
				[]byte("hello"),
				2030400,
				&environment.Context{},
			},
			[]byte("hello"),
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &dataCopy{}
			got, err := c.Execute(tt.args.in, tt.args.version, tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("Execute() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Execute() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dataCopy_GasCost(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want uint64
	}{
		{
			"test gas",
			args{
				[]byte("hello"),
			},
			18,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &dataCopy{}
			if got := c.GasCost(tt.args.input); got != tt.want {
				t.Errorf("GasCost() = %v, want %v", got, tt.want)
			}
		})
	}
}
