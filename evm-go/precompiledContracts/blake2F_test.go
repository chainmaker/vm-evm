package precompiledContracts

import (
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/environment"
	"encoding/hex"
	"encoding/json"
	"os"
	"reflect"
	"testing"
)

func Test_blake2F_Execute(t *testing.T) {
	type testCase struct {
		Name    string
		Args    string
		Want    string
		WantErr bool
	}

	var cases []*testCase

	data, err := os.ReadFile("blake2f.json")
	if err != nil {
		t.Fatal(err)
	}

	if err := json.Unmarshal(data, &cases); err != nil {
		t.Fatal(err)
	}
	for _, tt := range cases {
		t.Run(tt.Name, func(t *testing.T) {
			b := &blake2F{}
			input, _ := hex.DecodeString(tt.Args)
			got, err := b.Execute(input, 3000000, &environment.Context{})
			if err != nil {
				t.Errorf("Execute() error = %v", err)
				return
			}

			ret := hex.EncodeToString(got)
			if !reflect.DeepEqual(ret, tt.Want) {
				t.Errorf("Execute() got = %v, want %v", ret, tt.Want)
			}
		})
	}
}
