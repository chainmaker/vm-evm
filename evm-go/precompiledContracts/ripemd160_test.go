package precompiledContracts

import (
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/environment"
	"encoding/hex"
	"reflect"
	"testing"
)

func Test_ripemd160hash_Execute(t *testing.T) {
	want, _ := hex.DecodeString("000000000000000000000000108f07b8382412612c048d07d13f814118445acd")
	type args struct {
		input   []byte
		version uint32
		ctx     *environment.Context
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			"test ripemd",
			args{
				[]byte("hello"),
				2030400,
				&environment.Context{},
			},
			want,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &ripemd160hash{}
			got, err := c.Execute(tt.args.input, tt.args.version, tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("Execute() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Execute() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ripemd160hash_GasCost(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want uint64
	}{
		{
			"test gas",
			args{
				[]byte("hello"),
			},
			720,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &ripemd160hash{}
			if got := c.GasCost(tt.args.input); got != tt.want {
				t.Errorf("GasCost() = %v, want %v", got, tt.want)
			}
		})
	}
}
