package precompiledContracts

import (
	"reflect"
	"testing"

	"chainmaker.org/chainmaker/common/v2/evmutils"
	"chainmaker.org/chainmaker/logger/v2"
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/environment"
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/params"
)

func Test_printLog(t *testing.T) {
	caller := evmutils.FromHexString("f89ef2542fb2d26e7d88ac791ee47f6b21ce12b2")
	log := logger.GetLogger(logger.MODULE_EVM)
	type args struct {
		input   []byte
		version uint32
		ctx     *environment.Context
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "test0",
			args: args{
				input:   []byte("0helloworld"),
				version: params.V2030500,
				ctx: &environment.Context{
					Message: environment.Message{
						Caller: caller,
					},
					Contract: environment.Contract{
						Version: "1.0",
					},
					EvmLog: log,
				},
			},
			want:    nil,
			wantErr: false,
		},
		{
			name: "test1",
			args: args{
				input:   []byte("1helloworld"),
				version: params.V2030500,
				ctx: &environment.Context{
					Message: environment.Message{
						Caller: caller,
					},
					Contract: environment.Contract{
						Version: "1.0",
					},
					EvmLog: log,
				},
			},
			want:    nil,
			wantErr: false,
		},
		{
			name: "test2",
			args: args{
				input:   []byte("2helloworld"),
				version: params.V2030500,
				ctx: &environment.Context{
					Message: environment.Message{
						Caller: caller,
					},
					Contract: environment.Contract{
						Version: "1.0",
					},
					EvmLog: log,
				},
			},
			want:    nil,
			wantErr: false,
		},
		{
			name: "test3",
			args: args{
				input:   []byte("3helloworld"),
				version: params.V2030500,
				ctx: &environment.Context{
					Message: environment.Message{
						Caller: caller,
					},
					Contract: environment.Contract{
						Version: "1.0",
					},
					EvmLog: log,
				},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := printLog(tt.args.input, tt.args.version, tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("printLog() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("printLog() got = %v, want %v", got, tt.want)
			}
		})
	}
}
